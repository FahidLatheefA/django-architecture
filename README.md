
## Introduction

Django is a Python-based web framework that allows us to quickly create web applications. Django takes care of much of the hassle of web development, so we can focus on writing our app without needing to reinvent the wheel.

## Django Architecture

Django is based on  Model-View-Template (MVT) pattern. MVT consists of 3 components.

**Model**: Model is responsible for maintaining the data structure of the project, through databases (PostgreSQL, MySQL etc.).

**View**: Views perform operations and return a response to the user. This response can be the HTML contents of a Web page, or a redirect, or a 404 error.

**Templates**: Templates is used to generate dynamic HTML pages by using Django's template system. For example, with the use of jinja2, we will be able to display dynamic content on our web pages.

![Django MVT](https://www.javatpoint.com/django/images/django-mvt-based-control-flow.png)

## Few Django Security Features

### XSS Attack Protection

XSS (**Cross Site Scripting**) attacks allow a user to inject client-side scripts into the browsers of other users.

Django does have protection against it by using a escape filter that converts potentially harmful HTML characters to unharmful ones.

### CSRF Protection
CSRF (**Cross-site request forgery**) attacks allow a malicious user to execute actions using the credentials of another user without that user’s knowledge or consent.

Django checks for a secret code in each POST request. This ensures that a malicious user cannot “replay” a form POST to our website and have another logged in user unknowingly submit that form.

### SQL Injection Protection
SQL injection is a type of attack where a malicious user can execute arbitrary SQL code on a database. This can result in records being deleted or data leakage.

Django’s querysets are protected from SQL injection since their queries are constructed using query parameterization.

## Django settings file

> DISCLAIMER: The Django version I used to generate the project is 2.2. These settings may or may not exist in future/previous versions of Django.


When we create a new project in Django, a new folder with our `project name` is created. The 3 files which are found in the folder are `settings.py`, `urls.py` and `wsgi.py`. 

`urls.py` configures our project urls, and `wsgi.py` configures  Django’s primary deployment platform, [WSGI](https://wsgi.readthedocs.io/en/latest/), which is used to deploy our project live.

The most important file among these three is the `settings.py` file, which is used for configuring all settings of our project. Here is a detailed explanation of what each line in `settings.py` does for our project.

### `settings.py` detailed explanation

**BASE_DIR**: This refers to the absolute path in which `manage.py` file exists. The manage.py is integral in creating new apps, running server, running django shell etc.

**SECRET_KEY**: It is used to provide cryptographic signing, and should be set to a unique, unpredictable value. It is used for saving cookies cache. If someone has the secret key, they will be able to modify cookies sent by the application.

**DEBUG**: This is a toggle that turns on/off debug mode. When in debug mode, we can get detailed error pages, which helps in fixing our application quicker. Moreover, the modifications are dynamically changed when debug is on without the need of manually restarting the server after each change.

Due to the detailed error messages which may expose a lot of important metadata, it is very dangerous to keep *DEBUG=True* in production and is not advised to do so.

**ALLOWED_HOSTS**: A list of strings representing the host/domain names that this Django site can serve.

**INSTALLED_APPS**: A list of strings designating all applications that are enabled in the Django Project. After creating new applications, we have to manually add the application to the Django Installed Apps. Moreover, other outside applications have to be added to the list, so that it works with our project (Eg: crispy_forms).

**MIDDLEWARE**:  Middleware is a framework of hooks into Django’s request/response processing. It’s a light, low-level “plugin” system for globally altering Django’s input or output. For example, Security Middleware is used to maintain the security of the application.

**ROOT_URLCONF**: This is a string representing the relative path to the `urls.py`.

**TEMPLATES**: Settings of Django Template Language used for our project.

**WSGI_APPLICATION**: Path to wsgi.py file.

**DATABASES**: A dictionary containing the settings for all databases to be used with Django. By default, Django uses SQLite as the database engine.

**AUTH_PASSWORD_VALIDATORS**: The list of validators that are used to check the strength of a user’s passwords. By default, no validation is performed and all passwords are accepted.

**LANGUAGE_CODE**: Represents the name of a language. For example `'en-us'`.

**TIME_ZONE**: Represents the timezone. For example `'UTC'`.

**USE_I18N**: A boolean that specifies whether Django’s translation system should be enabled. This provides a way to turn it off, for performance. If this is set to `False`, Django will make some optimizations so as not to load the translation machinery.

**USE_L10N**: A boolean that specifies if localized formatting of data will be enabled by default or not. If this is set to `True`, Django will display numbers and dates using the format of the current locale.

**USE_TZ**: A boolean that specifies if datetimes will be timezone-aware by default or not. If this is set to `True`, Django will use timezone-aware datetimes internally.

**STATIC_ROOT**: The absolute path to the directory where `./manage.py collectstatic` will collect static files for deployment. So we have to only serve this folder while deploying.

**STATIC_URL**: The `URL` of which the static files in `STATIC_ROOT` directory are served.

## References
[Django Javatpoint](https://www.javatpoint.com/django)

[Django Official Documentation](https://docs.djangoproject.com/en/2.2/)
